package rabbit_wrapper

import "github.com/streadway/amqp"

type (
	Sender interface {
		Send(data []byte) error
	}

	Receiver interface {
		Receive() (<-chan amqp.Delivery, error)
	}

	Channel interface {
		Channel() *amqp.Channel
	}

	RabbitWrapper struct {
		queueName string
		ch        *amqp.Channel
	}
)

func (r *RabbitWrapper) Send(data []byte) error {
	return r.ch.Publish(
		"",
		r.queueName,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        data,
		})
}

func (r *RabbitWrapper) Receive() (<-chan amqp.Delivery, error) {
	return r.ch.Consume(
		r.queueName, // queue
		"",          // consumer
		true,        // auto-ack
		false,       // exclusive
		false,       // no-local
		false,       // no-wait
		nil,         // args
	)
}

func (r *RabbitWrapper) Channel() *amqp.Channel {
	return r.ch
}

func New(connectionString, queueName string) (*RabbitWrapper, error) {
	conn, err := amqp.Dial(connectionString)
	if err != nil {
		return nil, err
	}

	ch, err := conn.Channel()
	if err != nil {
		return nil, err
	}

	_, err = ch.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		return nil, err
	}

	return &RabbitWrapper{ch: ch, queueName: queueName}, nil
}
